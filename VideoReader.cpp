#include "VideoReader.hpp"
#include <cmath>
#include <iostream>
#include <mutex>
#include <queue>
#include <stack>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <thread>
#include <unistd.h>

using namespace cv;
using namespace std;
VideoReader::VideoReader(string url, int width, int height, Rect cropRect,
                         queue<FrameData> &queue_)
    : frameQueue(queue_) {
  this->url = url;
  this->width = width;
  this->height = height;
}

void videoreader_http(string url, int width, int height,
                      queue<FrameData> &frameQueue) {
  VideoCapture cap;
  int nframe = 0;
  while (1) {
    this_thread::sleep_for(chrono::milliseconds(1));
    string urlsearchip = url;
    cout << urlsearchip << endl;
    cap.open(urlsearchip);
    int delay = 1000 / 25;
    if (cap.isOpened()) {
      long long id = 0;
      while (1) {
        this_thread::sleep_for(chrono::milliseconds(1));
        //        auto startime = CLOCK_NOW();
        id++;
        FrameData thisFrame;
        cap >> thisFrame.frame;
//        cv::imshow("Plate1", thisFrame.frame);

        if (thisFrame.frame.data) {
          if (id % 2 == 0) {
            resize(thisFrame.frame, thisFrame.frame, cv::Size(width, height));
            thisFrame.frameID = id;
            frameQueue.push(thisFrame);
          }
        } else
          break;

        // ToDO: printf Framefps
        nframe++;
        //        auto end = CLOCK_NOW();
//         cv::waitKey(1);
      }
    } else {
      cout << "\nLost connect!";
      this_thread::sleep_for(chrono::milliseconds(5000));
    }
  }
}

void VideoReader::operator()() {
  videoreader_http(url, width, height, frameQueue);
}
