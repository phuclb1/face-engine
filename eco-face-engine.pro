TEMPLATE = app
CONFIG += c++11
CONFIG -= app_bundle
CONFIG -= qt

DEFINES += USE_CAFFE

HEADERS += \
    VideoReader.hpp \
    define.hpp \
    FaceDetector.hpp \
    FaceRecognition.hpp \
    Tracker.hpp
SOURCES += main.cpp \
    VideoReader.cpp \
    FaceDetector.cpp \
    FaceRecognition.cpp \
    Tracker.cpp

INCLUDEPATH+= /usr/local/include
INCLUDEPATH+= /usr/local/include/opencv2
INCLUDEPATH+= /usr/local/cuda-9.2/include
INCLUDEPATH+= $$PWD/dlib
INCLUDEPATH+= $$PWD/caffe/build/src $$PWD/caffe/include
#INCLUDEPATH+= /usr/include
LIBS += -L/usr/local/lib -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_video -lopencv_bgsegm -lopencv_imgcodecs -lopencv_videoio -lopencv_objdetect
LIBS += -lpthread

LIBS += -lcudart -lcublas -lcurand -lglog -lgflags
LIBS += -lboost_system -lboost_filesystem -lboost_program_options -lboost_thread -lboost_date_time -lm -lhdf5_hl -lhdf5
LIBS += -L$$PWD/caffe/build/lib -lcaffe




