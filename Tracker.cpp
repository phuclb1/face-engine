#include "Tracker.hpp"


/**
 * @brief SingleTracker::SingleTracker Create new track. Match with one object
 * @param img_
 * @param _rect
 */
SingleTracker::SingleTracker(dlib::array2d<unsigned char> &img_, dlib::rectangle rect_,
                int track_id_); {
  this->track_id = track_id_;
  this->set_dlib_rec(rect_);
  this->set_cv_rec(rect_);
  this->set_center(rect_);
  this->tracker.start_track(img_, rect_);
  this->track_name = "";
  this->track_name.clear();
  this->lost_track_count = 0;
  // this->noFaceFoundFrameNumber = 0;
  this->trace = 0;

}

/**

/**
 * @brief SingleTracker::do_single_track update existed track
 * @param img_
 */

void SingleTracker::do_single_track(dlib::array2d<unsigned char> &img_) {
  trackerLogger << LDEBUG << "[START] do_single_track   " << trackId << " ----- "
                << track_name.c_str();
  this->track_quality = this->tracker.update(img_);
  dlib::rectangle pos = this->tracker.get_position();
  this->dlib_rec = pos;
  //  this->movingDistance = std::sqrt(
  //      std::pow(pos.tl_corner().x() + (pos.width() / 2) - center.x(), 2) +
  //      std::pow(pos.tl_corner().y() + (pos.height() / 2) - center.y(), 2));
  this->set_center(pos);
  this->isNeedDelete = this->is_need_to_deleted();
  // cout << "Moving distance " << this->movingDistance << endl;
}

/**
 * @brief SingleTracker::do_single_track update existed track
 * @param img_
 */

void SingleTracker::do_single_track_with_newbox(dlib::array2d<unsigned char> &img_,
                                            dlib::rectangle newBox) {

  this->tracker.start_track(img_, newBox);
  this->track_quality = 20;
  this->dlib_rec = newBox;
  this->set_center(newBox);
  this->lost_track_count = 0;

  this->isNeedDelete = this->is_need_to_deleted();
  // cout << "Moving distance " << this->movingDistance << endl;
}


/**
 * @brief SingleTracker::is_need_to_deleted Check condition to delete track in next
 * 15 frames
 * @return true/false
 */

bool SingleTracker::is_need_to_deleted() {
  bool res = false;
  if (this->gettrack_quality() > 8) {
    return false;
  } else {
    return true;
    //    this->lostTrackCount++;
    //    if (this->lostTrackCount > 10)
    //      res = true;
    //    cout << lostTrackCount << " xxxx " << lostTrackCountThreshold << " xxx
    //    "
    //         << track_quality << endl;
  }
  return res;
}

/**
 * @brief SingleTracker::is_deleted_next_frame Detele in next frame
 * @return
 */

bool SingleTracker::is_deleted_next_frame() {
  if (this->is_need_delete) {
    if (this->lost_track_count == 1)
      return true;
    else
      this->lost_track_count++;
  }
  return false;
}

/**
 * @brief SingleTracker::is_match_with_new_track Check new track match with existed
 * track or not
 * @param _rect
 * @return
 */

bool SingleTracker::is_match_with_new_track(dlib::rectangle _rect) {
  long x_bar = _rect.left() + _rect.width() * 0.5;
  long y_bar = _rect.top() + _rect.height() * 0.5;
  drectangle pos = this->dlib_rec;
  dlib::point center = this->center;

  if ((pos.left() <= x_bar) && (x_bar <= pos.right()) && (pos.top() <= y_bar) &&
      (y_bar <= pos.bottom()) && (_rect.left() <= center.x()) &&
      (center.x() <= _rect.right()) && (_rect.top() <= center.y()) &&
      (center.y() <= _rect.bottom())) {
    return true;
  } else
    return false;
}


/**
 * @brief TrackerManager::find_matched_tracker Find matched new track with track
 * list
 * @param _rect
 * @return
 */

int TrackerManager::find_matched_tracker(dlib::rectangle _rect) {
  trackerLogger << LDEBUG << "[START] TrackerManager::find_matched_tracker";
  int res = -1;
  if (this->track_list.size() > 0) {
    for (int i = 0; i < this->track_list.size(); ++i) {
      if (this->track_list[i]->is_match_with_new_track(_rect))
        res = this->track_list[i]->get_track_id();
    }
  }
  return res;
}

/**
 * @brief TrackerManager::insert_tracker insert new track into track list
 * @param img_
 * @param _rect
 */

void TrackerManager::insert_tracker(dlib::array2d<unsigned char> &img_,
                                   dlib::rectangle _rect) {
  trackerLogger << LDEBUG << "[START] TrackerManager::insert_tracker";
  int match_id = this->find_matched_tracker(_rect);

  if (match_id == -1) {

    this->last_track_id++;
    std::shared_ptr<SingleTracker> new_tracker(
        new SingleTracker(img_, _rect, last_track_id));
    this->track_list.push_back(newTracker);

  } else {
    for (int i = 0; i < this->track_list.size(); ++i) {
      if (this->track_list[i]->get_track_id() == match_id)
        this->track_list[i]->do_single_track_with_newbox(img_, _rect);
    }
  }
  trackerLogger << LDEBUG << "[END] TrackerManager::insert_tracker";
}



void TrackerManager::update_tracker(dlib::array2d<unsigned char> &img_) {
  // update track

  std::vector<std::thread> threadPool;
  std::for_each(track_list.begin(), track_list.end(),
                [&](std::shared_ptr<SingleTracker> ptr) {
                  threadPool.emplace_back(
                      [ptr, &img_]() { ptr->do_single_track(img_); });
                });

  for (int i = 0; i < threadPool.size(); i++)
    threadPool[i].join();

  // Check for delete

  for (int i = 0; i < track_list.size(); ++i) {
    this->track_list[i]->trace++;
    if (this->track_list[i]->is_deleted_next_frame()) {
      this->deleteTracker(this->track_list[i]->get_track_id());
    }
  }
}

/**
 * @brief TrackerManager::findTracker Find track by trackId
 * @param _trackId
 * @return
 */

int TrackerManager::findTracker(int _trackId) {
  trackerLogger << LDEBUG << "[START] TrackerManager::findTracker";
  auto target =
      std::find_if(this->track_list.begin(), this->track_list.end(),
                   [&, _trackId](std::shared_ptr<SingleTracker> ptr) -> bool {
                     return ptr->get_track_id() == _trackId;
                   });

  if (target == this->track_list.end())
    return -1;
  else
    return target - this->track_list.begin();
}

/**
 * @brief TrackerManager::deleteTracker Delete track by trackId
 * @param _trackId
 */

void TrackerManager::deleteTracker(int _trackId) {
  trackerLogger << LDEBUG << "[START] TrackerManager::deleteTracker "
                << _trackId;
  int result_idx = this->findTracker(_trackId);

  if (result_idx != -1) {

    this->track_list[result_idx].reset();

    this->track_list.erase(track_list.begin() + result_idx);
    //    cout << "Delete " << result_idx << endl;
  }
}

const string currentDateTime() {
  time_t now = time(0);
  struct tm tstruct;
  char buf[80];
  tstruct = *localtime(&now);
  // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
  // for more information about date/time format
  strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

  return buf;
}
