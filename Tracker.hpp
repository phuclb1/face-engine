#ifndef DTRACKER_HPP
#define DTRACKER_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include "Define.hpp"
#include "jsoncpp/json/json.h"

#include <boost/shared_ptr.hpp>
#include <dlib/dnn.h>
#include <dlib/image_processing.h>
#include <dlib/opencv.h>
using namespace dlib;
using namespace std;

/**
 * @brief The SingleTracker class
 * Represent one Face object
 */

class SingleTracker {
public:
  correlation_tracker tracker;
  int track_id;
  string track_name;
  dlib::rectangle dlib_rec;
  cv::Rect cv_rec;
  dlib::point center;
  double track_quality;

  bool is_need_delete;
  int lost_track_count;

public:
  SingleTracker(dlib::array2d<unsigned char> &img_, dlib::rectangle rect_,
                int track_id_);
//  SingleTracker(dlib::array2d<unsigned char> &_img, dlib::rectangle _rect,
//                int _trackId, std::vector<dlib::dpoint> shape);
  /*Get method*/
  int get_track_id() { return this->track_id; }

  string get_track_name() { return this->track_name; }

  dlib::rectangle get_dlib_rec() { return this->dlib_rec; }

  cv::Rect get_cv_rec() {return this->cv_rec;}

  dlib::point get_center() { return this->center; }

  double get_track_quality() { return this->track_quality; }

  /*Set method*/

  void set_track_id(int track_id_) { this->track_id = track_id_; }

  void set_track_name(string track_name_) { this->track_name = track_name_; }

  void set_dlib_rec(dlib::drectangle dlib_rec_) { this->dlib_rec = dlib_rec_; }

  void set_cv_rec(dlib::drectangle dlib_rec_) { 
    this->cv_rec = cv::Rect(dlib_rec_.tl_corner().x(),
                    dlib_rec_.tl_corner().y(),dlib_rec_.width(),dlib_rec_.height()); 
                    }

  void set_center(dlib::drectangle dlib_rec_) {
    this->center =
        dlib::point((long)(dlib_rec_.tl_corner().x() + (dlib_rec_.width() / 2)),
                    (long)(dlib_rec_.tl_corner().y() + (dlib_rec_.height() / 2)));
  }

  void set_track_quality(double track_quality_) {
    this->track_quality = track_quality_;
  }

  /* Core Function */
  void do_single_track(dlib::array2d<unsigned char> &img_);
  void do_single_track_with_newbox(dlib::array2d<unsigned char> &img_,
                               dlib::rectangle new_box);

  bool is_need_to_deleted();

  bool is_deleted_next_frame();

  bool is_match_with_new_track(dlib::rectangle rect_);
};

/**
 * @brief The TrackerManager class
 * Manage all single track
 */

class TrackerManager {
public:
//  std::function<void(std::string jsonData)> SendDataFunction;
  std::vector<std::shared_ptr<SingleTracker>> trackList;
  long numberOfPeople = 0;
//  std::queue<SendData> data_queue;
  int last_track_id = 0;
  cv::Rect roi;
  cv::Mat frame;
  std::string cam_id;
  long ts;


public:
  TrackerManager() {}
//  TrackerManager(std::string _placeId) { placeId = _placeId; }
  /* Get Function */
  std::vector<std::shared_ptr<SingleTracker>> &get_tracker_list() {
    return this->track_list;
  }
//  std::queue<SendData> &getDataQueue() { return this->data_queue; }
//  int getNumberOfPeople() { return this->numberOfPeople; }

  /* Core Function */

  int find_matched_tracker(dlib::rectangle rect_);

  void insert_tracker(dlib::array2d<unsigned char> &rect_, dlib::rectangle rect_);
//  void insertTracker(dlib::array2d<unsigned char> &_img, dlib::rectangle _rect,
//                     std::vector<dlib::dpoint> d);

  void update_tracker(dlib::array2d<unsigned char> &rect_);

  int find_tracker(int track_id_);

  void delete_tracker(int track_id_);

};

#endif // DTRACKER_HPP
