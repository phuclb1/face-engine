#ifndef FACERECOGNITION_HPP
#define FACERECOGNITION_HPP
#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <dlib/data_io.h>
#include <dlib/dnn.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_processing.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/opencv.h>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>

#include <X11/Xlib.h>
#include <fstream>
#include <functional>
#include <iostream>
#include <jsoncpp/json/json.h>
#include <map>
#include <queue>
#include <string>
#include <thread>
#include <vector>

using namespace cv;
using namespace std;
using namespace dlib;

class FaceRecognition
{
private:
    dlib::shape_predictor landmark_predictor;
    anet_type net;

public:
    FaceRecognition();
};

#endif // FACERECOGNITION_HPP