#ifndef DEFINE_HPP
#define DEFINE_HPP

#include <dlib/dnn.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;
using namespace dlib;

//-----Define-CNN

//---------------CNN-4-DETECT----------------------------------------------------
// —------------------------------------------------------------------------------------—
// A 5x5 conv layer that does 2x downsampling
template <long num_filters, typename SUBNET>
using con5d = con<num_filters, 5, 5, 2, 2, SUBNET>;
// A 5x5 conv layer that doesn't do any downsampling
template <long num_filters, typename SUBNET>
using con5 = con<num_filters, 5, 5, 1, 1, SUBNET>;
// For testing net
template <typename SUBNET>
using downsampler = relu<affine<
    con5d<32, relu<affine<con5d<32, relu<affine<con5d<16, SUBNET>>>>>>>>>;
template <typename SUBNET>
using downsampler_tr = relu<bn_con<
    con5d<32, relu<bn_con<con5d<32, relu<bn_con<con5d<16, SUBNET>>>>>>>>>;
template <typename SUBNET> using rcon5 = relu<affine<con5<45, SUBNET>>>;
template <typename SUBNET> using rcon5_tr = relu<bn_con<con5<45, SUBNET>>>;
using cnnnet_type = loss_mmod<
    con<1, 9, 9, 1, 1, rcon5<rcon5<rcon5<downsampler<
                           input_rgb_image_pyramid<pyramid_down<6>>>>>>>>;
using cnnnet_type_tr = loss_mmod<
    con<1, 9, 9, 1, 1, rcon5_tr<rcon5_tr<rcon5_tr<downsampler_tr<
                           input_rgb_image_pyramid<pyramid_down<6>>>>>>>>;

// —------------------------------------------------------------------------------------—

// A 3x3 conv layer that doesn't do any downsampling
template <long num_filters, typename SUBNET>
using con3 = con<num_filters, 3, 3, 1, 1, SUBNET>;
// Now we can define the 8x downsampling block in terms of conv5d blocks.  We
// also use relu and batch normalization in the standard way.
// For training net
template <typename SUBNET>
using downsampler_train = relu<bn_con<
    con5d<32, relu<bn_con<con5d<32, relu<bn_con<con5d<32, SUBNET>>>>>>>>>;
template <typename SUBNET>
using downsampler_test = relu<affine<
    con5d<32, relu<affine<con5d<32, relu<affine<con5d<32, SUBNET>>>>>>>>>;

// The rest of the network will be 3x3 conv layers with batch normalization and
// relu.  So we define the 3x3 block we will use here.
template <typename SUBNET> using rcon3_train = relu<bn_con<con3<32, SUBNET>>>;
template <typename SUBNET> using rcon3_test = relu<affine<con3<32, SUBNET>>>;

// Finally, we define the entire network.   The special input_rgb_image_pyramid
// layer causes the network to operate over a spatial pyramid, making the
// detector
// scale invariant.
using cnnnet_type_train = loss_mmod<
    con<1, 6, 6, 1, 1, rcon3_train<rcon3_train<rcon3_train<downsampler_train<
                           input_rgb_image_pyramid<pyramid_down<6>>>>>>>>;
using cnnnet_type_test = loss_mmod<
    con<1, 6, 6, 1, 1, rcon3_test<rcon3_test<rcon3_test<downsampler_test<
                           input_rgb_image_pyramid<pyramid_down<6>>>>>>>>;

//---------------CNN-4-DETECT----------------------------------------------------

//---------------CNN convert vecto ------------------
// Also, the input layer was locked to images of size 150.
template <template <int, template <typename> class, int, typename> class block,
          int N, template <typename> class BN, typename SUBNET>
using residual = add_prev1<block<N, BN, 1, tag1<SUBNET>>>;

template <template <int, template <typename> class, int, typename> class block,
          int N, template <typename> class BN, typename SUBNET>
using residual_down =
    add_prev2<avg_pool<2, 2, 2, 2, skip1<tag2<block<N, BN, 2, tag1<SUBNET>>>>>>;

template <int N, template <typename> class BN, int stride, typename SUBNET>
using block =
    BN<con<N, 3, 3, 1, 1, relu<BN<con<N, 3, 3, stride, stride, SUBNET>>>>>;

template <int N, typename SUBNET>
using ares = relu<residual<block, N, affine, SUBNET>>;
template <int N, typename SUBNET>
using ares_down = relu<residual_down<block, N, affine, SUBNET>>;

template <typename SUBNET> using alevel0 = ares_down<256, SUBNET>;
template <typename SUBNET>
using alevel1 = ares<256, ares<256, ares_down<256, SUBNET>>>;
template <typename SUBNET>
using alevel2 = ares<128, ares<128, ares_down<128, SUBNET>>>;
template <typename SUBNET>
using alevel3 = ares<64, ares<64, ares<64, ares_down<64, SUBNET>>>>;
template <typename SUBNET> using alevel4 = ares<32, ares<32, ares<32, SUBNET>>>;

using anet_type = loss_metric<fc_no_bias<
    128,
    avg_pool_everything<alevel0<alevel1<alevel2<alevel3<alevel4<max_pool<
        3, 3, 2, 2,
        relu<affine<con<32, 7, 7, 2, 2, input_rgb_image_sized<150>>>>>>>>>>>>>;


struct FrameData {
  long long frameID;
  cv::Mat frame;
  std::string frameTime;
};

struct FaceData {
  long long frameID;
  cv::Mat frame;
  long long trackId;
  cv::Mat face;
};

dlib::matrix<dlib::rgb_pixel> convert_mat2dlib(cv::Mat image) {
  dlib::cv_image<bgr_pixel> cv_temp(image);
  dlib::matrix<dlib::rgb_pixel> spimg;
  dlib::assign_image(spimg, cv_temp);
  return spimg;
}



#endif // DEFINE_HPP
